class node:
    def __init__(self) -> None:
        self.sym = ''
        self.pro = 0.0
        self.arr = [0] * 20
        self.top = 0


def shannon(start, h, list_with_probability):
    pack1 = 0
    pack2 = 0
    if (start + 1) == h or start == h or start > h:
        if start == h or start > h:
            return
        list_with_probability[h].top += 1
        list_with_probability[h].arr[list_with_probability[h].top] = 0
        list_with_probability[start].top += 1
        list_with_probability[start].arr[list_with_probability[start].top] = 1
        return
    else:
        for i in range(start, h):
            pack1 = pack1 + list_with_probability[i].pro
        pack2 = pack2 + list_with_probability[h].pro
        diff1 = pack1 - pack2
    if diff1 < 0:
        diff1 = diff1 * -1
    j = 2
    while j != h - start + 1:
        k = h - j
        pack1 = pack2 = 0
        for i in range(start, k + 1):
            pack1 = pack1 + list_with_probability[i].pro
        for i in range(h, k, -1):
            pack2 = pack2 + list_with_probability[i].pro
        diff2 = pack1 - pack2
        if diff2 < 0:
            diff2 = diff2 * -1
        if diff2 >= diff1:
            break
        diff1 = diff2
        j += 1
    k += 1
    for i in range(start, k + 1):
        list_with_probability[i].top += 1
        list_with_probability[i].arr[list_with_probability[i].top] = 1
    for i in range(k + 1, h + 1):
        list_with_probability[i].top += 1
        list_with_probability[i].arr[list_with_probability[i].top] = 0
    # Invoke Shannon function
    shannon(start, k, list_with_probability)
    shannon(k + 1, h, list_with_probability)


def probability_sort(n, list_with_probability):
    temp = node()
    for j in range(1, n):
        for i in range(n - 1):
            if list_with_probability[i].pro > list_with_probability[i + 1].pro:
                temp.pro = list_with_probability[i].pro
                temp.sym = list_with_probability[i].sym
                list_with_probability[i].pro = list_with_probability[i + 1].pro
                list_with_probability[i].sym = list_with_probability[i + 1].sym
                list_with_probability[i + 1].pro = temp.pro
                list_with_probability[i + 1].sym = temp.sym


def fano_code():
    total = 0
    nodes = [node() for _ in range(100)]
    with open("fano.txt", "r") as file:
        list_with_symbols = file.readline().split()
        list_with_probability = list(map(lambda x: float(x.replace(",", ".")), file.readline().split()))
    n = len(list_with_symbols)
    i = 0
    # Input symbols in nodes
    for i in range(n):
        # Insert the symbol to node
        nodes[i].sym += list_with_symbols[i]
    for i in range(n):
        # Insert the value to node
        nodes[i].pro = list_with_probability[i]
        total = total + nodes[i].pro
        # checking max probability
        if total > 1:
            raise ValueError
    i += 1
    nodes[i].pro = 1 - total
    probability_sort(n, nodes)
    for i in range(n):
        nodes[i].top = -1
    shannon(0, n - 1, nodes)
    result_dict = {}
    for i in range(n):
        result_dict[nodes[i].sym] = ''.join(str(e) for e in (nodes[i].arr[0: nodes[i].top + 1]))
    b = ''
    for i in result_dict:
        b += str(result_dict[i])
    print('fano coder result:', b)
    return result_dict


if __name__ == '__main__':
    fano_code()
