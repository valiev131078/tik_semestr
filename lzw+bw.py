from operator import itemgetter


def lzw_decode(str_to_decode_binary, A):
    list_to_decode = list(map(lambda x: int(x, 2), str_to_decode_binary))
    result = ""
    previous_word = A[list_to_decode.pop(0)]
    result += previous_word
    for k in list_to_decode:
        if k in A:
            word = A[k]
        else:
            word = previous_word + previous_word[0]
        result += word
        A[len(A)] = previous_word + word[0]
        previous_word = word
    return result


def bw_restore(I, L):
    n = len(L)
    X = sorted([(i, x) for i, x in enumerate(L)], key=itemgetter(1))
    T = [None for i in range(n)]
    for i, y in enumerate(X):
        j = y[0]
        T[j] = i
    Tx = [I]
    for i in range(1, n):
        Tx.append(T[Tx[i - 1]])
    S = [L[i] for i in Tx]
    S.reverse()
    return ''.join(S)


def from_console():
    A = {}
    words = ['00000000', '00000001', '00000010', '00000011', '00000110', '00000000', '00000110', '00000101', '00000000']
    In_1 = input('Введите ключи словаря: ').split()
    In_2 = input('Введите значения ключей: ').split()
    for i in range(len(In_2)):
        A[int(In_2[i])] = In_1[i]
    lzw = lzw_decode(words, A)
    print(lzw)
    print(bw_restore(1, lzw))


if __name__ == '__main__':
    from_console()
